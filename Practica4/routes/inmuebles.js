// Manejador de rutas para /inmuebles
const express = require('express');
const md5 = require('md5');
const Inmueble = require('../models/inmueble');
const Tipo = require('../models/tipo');
const passport = require('passport');

let router = express.Router();

//Ruta principal para listar todos los inmuebles
router.get('/', (req, res) => {
    Inmueble.find().populate('tipo').then(resultado => {
        res.render('listar_inmuebles', {
            inmuebles: resultado
        })
    }).catch(error => {
        console.log(error);
        res.render('listar_inmuebles', {
            inmuebles: []
        });
    });
});

// Ruta que recibe parámetros mediante Query string para filtrar inmuebles
router.get('/filtrar', (req, res) => {
    console.log(req.query);
    let precio = req.query.precio;
    let superficie = req.query.superficie;
    let numHabitaciones = req.query.numHabitaciones;
    console.log(precio + ' ' + superficie + ' ' + numHabitaciones);

    let query = Inmueble.find();

    if (precio > 0)
        query = query.where('precio').lt(precio);

    if (superficie > 0)
        query = query.where('superficie').lt(superficie);

    if (numHabitaciones > 0)
        query = query.where('numHabitaciones').lt(numHabitaciones);

    query.then(resultado => {
        res.render('listar_inmuebles', {
            inmuebles: resultado
        });
    }).catch(error => {
        console.log(error);
        res.render('listar_inmuebles', {
            inmuebles: []
        });
    });
})

// Ruta para acceder a la ficha de un inmueble concreto
router.get('/:id', (req, res) => {
    let id = req.params.id;
    Inmueble.findById(id).populate('tipo').then(resultado => {
        console.log(resultado);
        res.render('ficha_inmueble', {
            inmueble: resultado,
            error: false
        });
    }).catch(error => {
        console.log(error);
        res.render('ficha_inmueble', {
            inmueble: [],
            error: 'No se ha encontrado el evento'
        });
    })
});

router.get('/editar/:id', passport.authenticate('jwt', {session: false, failureRedirect: '/prohibido'}), (req, res) => {
    let id = req.params.id;
    console.log(id);
    Inmueble.findById(id).populate('tipo').then(resultado => {
        res.render('crear_inmueble', {
            inmueble: resultado,
            error: false
        });
    })
})

//Ruta para filtrar inmuebles por tipo
router.get('/tipo/:idTipo', (req, res) => {
    let tipoInmueble = req.params.idTipo;
    //Expresión regular para hacer la búsqueda de tipos insensible a mayúsculas
    Tipo.findOne({
        nombre: new RegExp(tipoInmueble, 'i')
    }).then(tipo => {
        console.log(tipo);
        Inmueble.find({
            tipo: tipo
        }).populate('tipos').then(resultado => {
            res.render('listar_inmuebles', {
                inmuebles: resultado
            })
        }).catch(error => {
            console.log(error);
            res.render('listar_inmuebles', {
                inmuebles: []
            });
        });
    });
})

//Ruta para crear nuevos inmuebles
router.post('/', passport.authenticate('jwt', {session: false, failureRedirect: '/prohibido'}), (req, res) => {
    if (!req.files)
        res.render('crear_inmueble', {
            error: 'No se ha seleccionado archivo',
            inmueble: {}
        });
    else {
        let fileName;
        console.log(req.body);
        let extension = req.files.imagen.mimetype === 'image/jpeg' ? '.jpg' : '.png';
        fileName = '/img/' + md5(req.files.imagen.name) + extension;
        req.files.imagen.mv(__dirname + '/../public' + fileName).then(err => {
            if (err) {
                console.log(err);
                res.render('crear_inmueble', {
                    error: 'Error subiendo archivo',
                    inmueble: {}
                });
            }
            Tipo.findOne({
                nombre: req.body.tipoInmueble
            }).then(tipoInmueble => {
                let inmueble = new Inmueble({
                    descripcion: req.body.descripcion,
                    tipo: tipoInmueble,
                    numHabitaciones: req.body.numHabitaciones,
                    superficie: req.body.superficie,
                    precio: req.body.precio,
                    imagen: fileName
                });

                inmueble.save().then(resultado => {
                    console.log("Guardado");
                    res.render('ficha_inmueble', {inmueble: inmueble, error: false})
                });
            })
        })
    }

})

//Ruta para eliminar un inmueble concreto
router.delete('/:id', passport.authenticate('jwt', {session: false, failureRedirect: '/prohibido'}), (req, res) => {
    let id = req.params.id;
    console.log(id);
    Inmueble.findByIdAndRemove(id).then(resultado => {
        res.send({ok: true});
    }).catch(err => {
        console.log(error);
        res.render('ficha_inmueble', {
            inmueble: resultado,
            error: 'No se ha podido borrar el inmueble'
        })
    });
});

//Ruta para editar eventos (No edita imágenes)
router.put('/:id', passport.authenticate('jwt', {session: false, failureRedirect: '/prohibido'}), (req, res) => {
    let id = req.params.id;
    let datosActualizados = req.body;
    console.log(datosActualizados);
    console.log(id);
    Inmueble.findByIdAndUpdate(id, datosActualizados).then(resultado => {
        console.log("Resultado find one and update");
        console.log(resultado);
        res.status(200).send();
    }).catch(error => {
        console.log(error);
    })
})

module.exports = router;