// Fichero para manejar la ruta raíz y los formularios
// para crear y filtrar inmuebles
const express = require('express');
const passport = require('passport');
let router = express.Router();

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/inicio', (req, res) => {
    res.render('inicio');
})

router.get('/nuevo_inmueble', passport.authenticate('jwt', {session: false, failureRedirect: '/prohibido'}), (req, res) => {
    res.render('crear_inmueble', {inmueble: {}, error: false});
})

router.get('/filtrar_inmuebles', (req, res) => {
    res.render('filtrar_inmuebles');
})

router.get('/login', (req, res) => {
    res.render('login');
});

router.get('/registro', (req, res) => {
    res.render('registro');
})

router.get('/prohibido', (req, res) => {
    console.log('Prohibido');
    res.render('prohibido');
})

router.post('/prohibido', (req, res) => {
    console.log('Prohibido');
    res.render('prohibido');
})

router.put('/prohibido', (req, res) => {
    console.log('Prohibido');
    res.render('prohibido');
})

router.delete('/prohibido', (req, res) => {
    console.log('Prohibido');
    res.sendStatus(403);
})

module.exports = router;