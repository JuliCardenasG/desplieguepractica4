// Archivo para manejar las peticiones de /tipos (No se usa en el cliente)
const express = require('express');
const Tipo = require('../models/tipo');

let router = express.Router();

router.get('/', (req, res) => {
    Tipo.find().then(tipos => {
        console.log(tipos);
        res.render('filtrar_inmuebles');
    });
});

module.exports = router;