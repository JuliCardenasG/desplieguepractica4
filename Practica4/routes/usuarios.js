//Manejador de rutas para usuarios
const express = require('express');
const md5 = require('md5');
const Usuario = require('../models/usuario');
const passport = require('passport');
const {
    Strategy,
    ExtractJwt
} = require('passport-jwt');
const jwt = require('jsonwebtoken');
const secreto = "DAW";

//Establecer la estrategia de autenticación local con Passport
passport.use(new Strategy({
    secretOrKey: secreto,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}, (payload, done) => {
    if (payload.id) {
        return done(null, {
            id: payload.id
        });
    } else {
        return done(new Error("Usuario incorrecto"), null);
    }
}));

let router = express.Router();

//Función para generar un token si el login es correcto
let generarToken = id => {
    return jwt.sign({
        id: id
    }, secreto, {
        expiresIn: "2 hours"
    });
}

//Ruta POST para el registro
router.post('/registro', (req, res) => {
    let datosUsuario = req.body;
    console.log(req.body);
    let usuario = new Usuario({
        nombre: datosUsuario.nombre,
        login: datosUsuario.login,
        password: md5(datosUsuario.password)
    });

    usuario.save().then(resultado => {
        res.send({ok: true});
    }).catch({ok: false, mensajeError: "No se pudo registrar"});
});

//Ruta POST para el login
router.post('/login', (req, res) => {
    console.log(req.body);
    Usuario.findOne({
            login: req.body.login,
            password: md5(req.body.password)
        })
        .then(resultado => {
            console.log(resultado);
            if (resultado)
                res.send({
                    ok: true,
                    token: generarToken(resultado._id)
                });
            else
                res.send({
                    ok: false,
                    mensajeError: "Usuario incorrecto"
                });
        }).catch(error => {
            console.log(error);
            res.send({
                ok: false,
                mensajeError: "Usuario incorrecto"
            });
        });
});

module.exports = router;