// Fichero que actúa de seed para poblar la colección de tipos en MongoDB
const mongoose = require('mongoose');
const Tipo = require('./models/tipo');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');

let piso = new Tipo({
    nombre: "Piso"
});
piso.save();

let bungalow = new Tipo({
    nombre: "Bungalow"
});

bungalow.save();

let atico = new Tipo({
    nombre: "Atico"
});
atico.save();

let chalet = new Tipo({
    nombre:"Chalet"
});
chalet.save();

let plantaBaja = new Tipo({
    nombre: "Planta baja"
});
plantaBaja.save();