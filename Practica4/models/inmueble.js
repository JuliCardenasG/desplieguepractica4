const mongoose = require('mongoose');
const Tipo = require('./tipo');

let inmuebleSchema = new mongoose.Schema({
    descripcion: {
        type: String,
        required: true,
        minlength: 10,
        trim: true
    },
    tipo: {
        type: mongoose.Schema.ObjectId,
        ref: 'Tipo',
        required: true
    },
    numHabitaciones: {
        type: Number,
        required: true,
        minlength: 1
    },
    superficie: {
        type: Number,
        required: true,
        minlength: 25
    },
    precio: {
        type: Number,
        required: true,
        min: 10000
    },
    imagen: {
        type: String,
        required: false
    }
})

let Inmueble = mongoose.model('inmueble', inmuebleSchema);

module.exports = Inmueble;