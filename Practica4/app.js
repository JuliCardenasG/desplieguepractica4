//Fichero principal del servidor Express
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const inmuebles = require('./routes/inmuebles');
const tipos = require('./routes/tipos');
const index = require('./routes/index');
const usuarios = require('./routes/usuarios');
const passport = require('passport');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');

let app = express();
app.use(passport.initialize());
app.use(fileUpload());
app.use(express.static('./public'));

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json({
    limit: '5mb'
}));

app.set('view engine', 'ejs');

app.use('/', index);
app.use('/tipos', tipos);
app.use('/inmuebles', inmuebles);
app.use('/usuarios', usuarios);

app.listen(8080);